import { createI18n } from 'vue-i18n';
import storage from '@/utils/storage';
import elementCnLocale from 'element-plus/dist/locale/zh-cn.mjs';
import elementEnLocale from 'element-plus/dist/locale/en.mjs';
import elementFrLocale from 'element-plus/dist/locale/fr.mjs';
import enLocale from './en.json';
import cnLocale from './cn.json';
import frLocale from './fr.json';

export const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale
  },
  cn: {
    ...cnLocale,
    ...elementCnLocale
  },
  fr: {
    ...frLocale,
    ...elementFrLocale
  }
  // or: {}
};
export function getLanguage() {
  const chooseLanguage = storage.getLanguage();
  if (chooseLanguage) {
    return chooseLanguage;
  } else {
    const language = navigator.language.toLowerCase();
    console.log(language);

    return language ? language.split('-')[1] : 'cn';
  }
}
const i18n = createI18n({
  fallbackLocale: 'or',
  globalInjection: true,
  legacy: false,
  locale: getLanguage(),
  messages
});
export default i18n;
