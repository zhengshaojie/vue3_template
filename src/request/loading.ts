import { ElLoading } from 'element-plus';
import { debounce } from 'lodash';
import 'element-plus/theme-chalk/el-loading.css';

let loading: any = null;
let RequestCount = 0;
export function startLoading() {
  if (RequestCount === 0 && !loading) {
    loading = ElLoading.service({
      lock: true,
      text: 'loading...',
      background: 'rgba(0, 0, 0, 0.7)'
    });
  }
  RequestCount++;
}

export function endLoading() {
  RequestCount--;
  if (RequestCount < 0) RequestCount = 0;
  if (RequestCount === 0) {
    toHideLoading();
  }
}

// 防抖：将 500ms 间隔内的关闭 loading 便合并为一次。防止连续请求时， loading闪烁的问题。
const toHideLoading = debounce(function () {
  if (loading && RequestCount === 0) {
    loading.close();
    loading = null;
  }
}, 500);
