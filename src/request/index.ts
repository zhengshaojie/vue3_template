import _ from 'lodash';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { startLoading, endLoading } from './loading';

export interface myAxiosRequestConfig extends AxiosRequestConfig {
  hiddenLoading?: boolean;
}

const service = axios.create({
  baseURL: '/api',
  // baseURL: import.meta.env.VITE_VUE_BASE_URL,
  timeout: 50000
});

service.interceptors.request.use(
  (config: myAxiosRequestConfig) => {
    if (!config.hiddenLoading) {
      startLoading();
    }
    if (localStorage.getItem('token') && config && config.headers) {
      config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  (response: AxiosResponse) => {
    endLoading();
    const res = response.data;
    if (!res.code || !_.isNumber(res.code)) {
      return res;
    }
    if (res.code !== 200) {
      window.$message(res.msg || res.message || 'Error');
      return Promise.reject(new Error(res.msg || res.message || 'Error'));
    } else {
      return res.data;
    }
  },
  (error) => {
    endLoading();
    if (error.message === 'Network Error') {
      window.$message('error', '访问网络不稳定，请稍后再试');
      return Promise.reject(error);
    }
    if (error.message.includes('timeout')) {
      window.$message('error', '连接服务器超时，请稍后再试');
      return Promise.reject(error);
    }
    window.$message('error', error.message);
    return Promise.reject(error);
  }
);

export default service;
