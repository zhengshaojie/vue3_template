import { createRouter, createWebHistory, Router, RouteRecordRaw } from 'vue-router';
import permission from './permission';
import Layout from '@/layout/index.vue';

// 静态路由
const constantRoutes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import('@/views/login/index.vue')
  },
  {
    path: '/home',
    redirect: '/home/welcome',
    component: Layout,
    children: [
      {
        path: 'welcome',
        component: () => import('@/views/home/index.vue')
      }
    ]
  }
];

const router: Router = createRouter({
  history: createWebHistory(),
  scrollBehavior: () => ({ top: 0, left: 0 }),
  routes: constantRoutes
});
// 动态路由添加(根据返回的资源进行路由的权限添加)
permission(router);
export default router;
