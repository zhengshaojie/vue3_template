import { Router, RouteRecordRaw } from 'vue-router';
import storage from '@/utils/storage';
import _ from 'lodash';
import { asyncRoutes } from './routes';
import { useAuthStore } from '@/store/modules/auth';

/**
 * 动态路由权限筛选
 * @param {*} routes 传入的需要筛选的动态路由表
 * @param {*} permissionRoutes gateway返回的权限路由表
 * @param {*} hasChildren 判断是否有子级路由
 * @param {*} parentRouter 在有子级路由的基础上循环递归传入找到该子级路由的父级路由
 */
function filterAsyncRoutes(
  routes: RouteRecordRaw[],
  permissionRoutes?: RouteRecordRaw[] | null,
  hasChildren = false,
  parentRouter?: RouteRecordRaw
) {
  const res: RouteRecordRaw[] = [];
  _.forEach(routes, (route) => {
    if (hasChildren) {
      const routeElement = _.find(parentRouter?.children, (t) => t.name === route.name);
      if (routeElement) {
        if (route.children) {
          route.children = filterAsyncRoutes(route.children, null, true, routeElement);
        }
        res.push(route);
      }
    } else {
      const routeElement = _.find(permissionRoutes, (t) => t.name === route.name);
      if (routeElement) {
        if (route.children) {
          route.children = filterAsyncRoutes(route.children, null, true, routeElement);
        }
        res.push(route);
      }
    }
  });
  return res;
}

export default function permission(router: Router) {
  router.beforeEach(async (to, from, next) => {
    if (to.path === '/login') {
      next();
    } else {
      const hasToken = storage.getToken();
      if (hasToken) {
        const authStore = useAuthStore();
        if (!authStore.routes.length) {
          console.log('need permission');
          await authStore.setRoutes();
          const routes = filterAsyncRoutes(asyncRoutes, authStore.routes);
          // 匹配不到的路由转发404 写在最后
          routes.push({ path: '/:pathMatch(.*)*', redirect: '/404' });
          for (const route of routes) {
            router.addRoute(route);
          }
          next({ ...to, replace: true });
        } else {
          next();
        }
      } else {
        next('/login');
      }
    }
  });
}
