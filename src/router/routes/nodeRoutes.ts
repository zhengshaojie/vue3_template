import Layout from '@/layout/index.vue';

export default {
  path: '/node',
  component: Layout,
  name: 'node',
  children: [
    {
      path: 'route',
      component: () => import('@/views/node/route/index.vue'),
      name: 'node.route'
    },
    {
      path: 'colicoli',
      component: () => import('@/views/node/colicoli/index.vue'),
      name: 'node.COLICOLI'
    }
  ]
};
