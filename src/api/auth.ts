import request, { myAxiosRequestConfig } from '@/request';

// 登录
export function login(data: any): Promise<any> {
  return request({
    url: '/auth/login',
    method: 'post',
    data,
    hiddenLoading: true
  } as myAxiosRequestConfig);
}

// 获取菜单资源
export function getMenu(): Promise<any> {
  return request({
    url: '/resources/menu/me',
    method: 'get',
    hiddenLoading: true
  } as myAxiosRequestConfig);
}
