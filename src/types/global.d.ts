// 键值对声明
declare type validKeyType = {
  [key: string]: boolean | number | string;
};