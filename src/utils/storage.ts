import _ from 'lodash';

export default {
  // 从本地缓存获取用户信息
  getAuth() {
    const auth = localStorage.getItem('userInfo');
    return auth ? JSON.parse(auth) : null;
  },
  // 从本地缓存获取token信息
  getToken() {
    return localStorage.getItem('userInfo');
  },
  // 从本地缓存获取language信息
  getLanguage() {
    return localStorage.getItem('language');
  },
  // 设置用户信息存入本地
  setAuth(auth: object | string) {
    localStorage.setItem('userInfo', _.isObject(auth) ? JSON.stringify(auth) : auth);
  },
  // 设置token存入本地
  setToken(token: string) {
    localStorage.setItem('token', token);
  },
  // 设置语言
  setLanguage(language: string) {
    localStorage.setItem('language', language);
  },
  // 清除本地缓存信息
  clearInfo() {
    localStorage.removeItem('userInfo');
    localStorage.removeItem('token');
  }
};
