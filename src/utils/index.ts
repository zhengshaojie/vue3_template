import XLSX from 'xlsx-js-style';
import _ from 'lodash';
import dayjs from 'dayjs';

// 用于检测是否是移动端设备
export function isMobile() {
  return !!navigator.userAgent.match(
    /(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i
  );
}

// 组合单元格设置样式
function organizeStyle(ws: any, condition?: any[]) {
  const keys = _.keys(ws); // 已有单元格
  _.forEach(condition, (c) => {
    // 遍历单元格样式数组
    _.forEach(c.key, (k) => {
      if (_.isString(k)) {
        // 判断传入的是否为字符串
        _.forEach(keys, (key) => {
          // 判断字符串中是否含有数字，如果含有则为一单元格，否则为一纵列
          if (_.words(k, /\d+/g).length === 0 && _.replace(key, /[^a-zA-Z]/g, '') === k) {
            ws[key].s = c.style;
          }
          if (key === k) {
            ws[key].s = c.style;
          }
        });
      } else {
        _.forEach(keys, (key) => {
          // 传入的key数组元素不是字符串则正则筛选出符合条件的单元格
          if (Number(_.replace(key, /[^\d]/g, '')) === k) {
            ws[key].s = c.style;
          }
        });
      }
    });
  });
  const commonStyle = {
    alignment: { vertical: 'center', horizontal: 'center' }
  };
  _.forEach(keys, (key) => {
    if (!_.includes(key, '!')) {
      if (ws[key].s && _.keys(ws[key].s).length !== 0) {
        ws[key].s = _.assign(ws[key].s, commonStyle);
      } else {
        ws[key].s = commonStyle;
      }
    }
  });
}

/* condition 引入格式(如果是多表的话则数组里边套数组)

let condition = [
  {
    key: [2, 'B3'],
    style: {
      fill: { bgColor: { rgb: 'DC143C' }, fgColor: { rgb: 'DC143C' } },
      font: { color: { rgb: 'ffffff' } },
    },
  },
  {
    key:['C4','A1'],
    style:{
      fill: { bgColor: { rgb: '7FFFAA' }, fgColor: { rgb: '7FFFAA' } },
      font: { color: { rgb: 'ffffff' } },
    }
  },
];

*/

// 导出Excel
export function exportExcel(data: any, fileName = 'demo.xlsx', merge: any[] = [], condition: any[] = []) {
  if (!data[0].sheetName) {
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.aoa_to_sheet(data);
    ws['!cols'] = [];
    if (merge.length !== 0) {
      ws['!merges'] = merge;
    }
    for (let i = 0; i < data[0].length; i++) {
      ws['!cols'].push({ wch: 20 });
    }
    if (condition.length !== 0) {
      organizeStyle(ws, condition);
    }
    XLSX.utils.book_append_sheet(wb, ws, '1');
    XLSX.writeFile(wb, fileName);
  } else {
    const wb = XLSX.utils.book_new();
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      const ws = XLSX.utils.aoa_to_sheet(element.data);
      ws['!cols'] = [];
      if (merge[i] && merge[i].length !== 0) {
        ws['!merges'] = merge[i];
      }
      if (element.data.length !== 0) {
        for (let i = 0; i < element.data[0].length; i++) {
          ws['!cols'].push({ wch: 20 });
        }
      }
      if (condition.length !== 0 && condition[i].length !== 0) {
        organizeStyle(ws, condition[i]);
      }
      XLSX.utils.book_append_sheet(wb, ws, element.sheetName);
    }
    XLSX.writeFile(wb, fileName);
  }
}

// 读取excel
export function readFile(files: File) {
  let ws = null;
  if (!/\.(xls|xlsx)$/.test(files.name.toLowerCase())) {
    window.$message.error('上传格式不正确，请上传xls或者xlsx格式');
    return false;
  }
  const fileReader = new FileReader();
  return new Promise((resolve, reject) => {
    fileReader.onload = (ev) => {
      try {
        const data = ev?.target?.result;
        const workbook = XLSX.read(data, {
          type: 'binary'
        });
        const wsname = workbook.SheetNames[0]; // 取第一张表
        ws = XLSX.utils.sheet_to_json(workbook.Sheets[wsname]); // 生成json表格内容
        if (ws.length === 0) {
          return window.$message.error('请确保所导入的数据至少有一条记录');
        }
        resolve(ws);
      } catch (e) {
        return false;
      }
    };
    fileReader.readAsBinaryString(files);
  });
}

// 转换法国时间(condition传入需要转化时间的字符串数组)
export function changeTimeToParis(item: any, condition: string[], formatIndex = 0) {
  const farmat: string[] = ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD HH:mm', 'YYYY-MM-DD', 'HH:mm'];
  _.forEach(condition, (i) => {
    if (item[i]) {
      const element: any = dayjs(item[i]);
      item[i] = element.tz('Europe/Paris').format(farmat[formatIndex]);
    }
  });
}
