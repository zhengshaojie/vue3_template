import { createApp } from 'vue';
import App from './App.vue';
import router from '@/router';
// 通用样式
import '@/style/index.scss';
import '@/style/common.scss';

// 将element-plus messages组件单独引入挂在到window上
import { ElMessage } from 'element-plus';
import 'element-plus/theme-chalk/base.css';
import 'element-plus/theme-chalk/el-message.css';
window.$message = ElMessage;

// pinia实例
import store from '@/store';
// i18n
import i18n from '@/lang';

const app = createApp(App).use(router).use(store).use(i18n);
app.mount('#app');
