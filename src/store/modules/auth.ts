import { defineStore } from 'pinia';
import { getMenu } from '@/api/auth';
import { useUserStore } from './user';
import router from '@/router';

export const useAuthStore = defineStore('auth', {
  state: () => {
    return {
      routes: [] as any[]
    };
  },
  actions: {
    async setRoutes() {
      try {
        const res = await getMenu();
        if (!res.length) {
          // 在权限返回为空的时候加一条空数据，防止下次路由进入再次空校验
          this.routes.push({});
        } else {
          this.routes = res;
        }
      } catch (error) {
        const userStore = useUserStore();
        userStore.clearInfo();
        router.replace('/login');
      }
    },
    clearRoutes() {
      this.routes = [];
    }
  }
});
