import { defineStore } from 'pinia';
import storage from '@/utils/storage';

export const useUserStore = defineStore('user', {
  state: () => {
    return {
      userInfo: storage.getAuth() || null,
      token: storage.getToken()
    };
  },
  actions: {
    // 登录设置用户信息和token
    setUserInfo(info: any) {
      storage.setAuth(info);
      storage.setToken(info.token);
      this.userInfo = info;
    },
    // 用户退出清除信息
    clearInfo() {
      storage.clearInfo();
    }
  }
});
