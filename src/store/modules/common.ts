import { defineStore } from 'pinia';
import storage from '@/utils/storage';

export const useCommonStore = defineStore('common', {
  state: () => {
    return {
      collapse: false,
      language: storage.getLanguage() || 'cn'
    };
  },
  actions: {
    // 菜单栏收缩
    setCollapse() {
      this.collapse = !this.collapse;
    },
    // 设置语言环境
    setLanguage(language: string) {
      this.language = language;
      storage.setLanguage(language);
    }
  }
});
