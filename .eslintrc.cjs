module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      tsx: true
    }
  },
  env: {
    node: true,
    'vue/setup-compiler-macros': true
  },
  globals: {
    jest: 'readonly',
    window: true
  },
  plugins: ['@typescript-eslint', 'prettier', 'import'],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-recommended',
    'prettier',
    './.eslintrc-auto-import.json'
  ],
  rules: {
    'no-var': 'error', // 禁止使用 var
    'no-mixed-spaces-and-tabs': 'error', // 不能空格与tab混用
    quotes: [2, 'single'], // 使用单引号
    'vue/multi-word-component-names': 'off', // 驼峰警告
    '@typescript-eslint/no-explicit-any': 'off', // 使用any类型警告
    '@typescript-eslint/no-unused-vars': 'off', // 关闭检测未使用的变量,交由tsconfig.json中noUnusedLocals决定
    'no-undef': 0, // 不能有未定义的变量
  }
};
